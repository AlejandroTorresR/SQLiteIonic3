import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite'; //https://ampersandacademy.com/tutorials/ionic-framework-version-2/sqlite-native-plugin-example


@Injectable()
export class TasksService {

	username='';
	public tasks: any = [];

	constructor(private sqlite: SQLite) {
	}

	createDatabase(){
		return this.sqlite.create({
			name: 'data.db',
			location: 'default'
		})
	}

	createTable(){
		(db: SQLiteObject) => {
			//data insert section
			let sql = 'CREATE TABLE IF NOT EXISTS usernameList(id INTEGER PRIMARY KEY AUTOINCREMENT,name)'
			db.executeSql(sql, {})
		}
	}

	getAll(){
		this.createDatabase().then((db: SQLiteObject) => {
			//data retrieve section
			db.executeSql('select * from usernameList', {}).then((data) => {
				console.log(JSON.stringify(data));
				this.tasks = [];
				if(data.rows.length > 0) {
					for(var i = 0; i < data.rows.length; i++) {
						this.tasks.push({name: data.rows.item(i).name, id: data.rows.item(i).id});
					}
				}
				console.log(JSON.stringify(this.tasks))
			}, (err) => {
				console.log('Unable to execute sql: '+JSON.stringify(err));
			});
		})
	}

	delete(task:any){
//		let idTask = task + 1;
		return this.createDatabase().then((db: SQLiteObject) => {
			db.executeSql('DELETE FROM usernameList WHERE id=?', [task.id]).then((data) => {
				console.log(JSON.stringify(data));
			}, (err) => {
				console.log('Unable to execute sql: '+JSON.stringify(err));
			});
		})
	}

	deleteDB(){
		this.createDatabase().then((db: SQLiteObject) => {
			db.executeSql('DROP TABLE usernameList', {}).then((data) => {
				this.tasks = [];
			})
		})
		
	}

	saving() {
		this.createDatabase().then((db: SQLiteObject) => {
			//data insert section
			db.executeSql('CREATE TABLE IF NOT EXISTS usernameList(id INTEGER PRIMARY KEY AUTOINCREMENT,name)', {})
			.then(() => console.log('Executed SQL'))
			.catch(e => console.log(e));

			//data insert section
			db.executeSql('INSERT INTO usernameList(name) VALUES(?)', [this.username])
			.then(() => console.log('Executed SQL'))
			.catch(e => console.log(e));

			//data retrieve section
			db.executeSql('select * from usernameList', {}).then((data) => {
				console.log(JSON.stringify(data));
				//alert(data.rows.length);
				//alert(data.rows.item(5).name);
				this.tasks = [];
				if(data.rows.length > 0) {
					for(var i = 0; i < data.rows.length; i++) {
						//alert(data.rows.item(i).name);�
						this.tasks.push({name: data.rows.item(i).name, id: data.rows.item(i).id});
					}
				}
			}, (err) => {
				alert('Unable to execute sql: '+JSON.stringify(err));
			});
		})
		.catch(e => console.log(JSON.stringify(e)));
		console.log(this.username);
	}

	save() {
		this.createDatabase()
		.then((db: SQLiteObject) => {

			//data insert section
			db.executeSql('INSERT INTO usernameList(name) VALUES(?)', [this.username])
			.then(() => console.log('Executed SQL'))
			.catch(e => console.log(e));

			//data retrieve section
			db.executeSql('select * from usernameList', {}).then((data) => {
				console.log(JSON.stringify(data));
				//alert(data.rows.length);
				//alert(data.rows.item(5).name);
				this.tasks = [];
				if(data.rows.length > 0) {
					for(var i = 0; i < data.rows.length; i++) {
						//alert(data.rows.item(i).name);�
						this.tasks.push({name: data.rows.item(i).name});
					}
				}
			}, (err) => {
				alert('Unable to execute sql: '+JSON.stringify(err));
			});
		})
		.catch(e => console.log(JSON.stringify(e)));
		console.log(this.username);
	}

		}