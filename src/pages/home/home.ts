import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { TasksService } from '../../providers/tasks-service'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  constructor(
    public tasksService: TasksService,
    public alertCtrl: AlertController,
  	public navCtrl: NavController
  	) {

  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Use this lightsaber?',
      message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  deleteTask(task: any, index) {
    let confirm = this.alertCtrl.create({
      title: 'Delete saved duel?',
      message: 'Do you agree to delete this saved duel?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: (data) => {
            console.log('Agree clicked', task);
            this.tasksService.delete(task)
            .then(() => {
              this.tasksService.tasks.splice(index, 1);
            })
            .catch( error => {
              console.error( error );
            })
          }
        }
      ]
    });
    confirm.present();
  }

}
